.386	
.MODEL flat,STDCALL

stala equ 10
STD_OUTPUT_HANDLE equ -11

ExitProcess PROTO : DWORD
GetStdHandle PROTO : DWORD
wsprintfA PROTO C :VARARG
WriteConsoleA PROTO :DWORD, :DWORD, :DWORD, :DWORD, :DWORD
.DATA
	cout			dd ?

	wynik		    dd ?
	zmiennaa		dd 2d
	zmiennab        dd 3d
	zmiennac	    dd 4d
	tekst			db "Wynik: %i",10,0
	rozmiar			dd	$ - tekst
	lZnakow			dw 0
	bufor			dd 128 DUP(?)
	rout			dd 0
.CODE
main proc 
	invoke GetStdHandle, STD_OUTPUT_HANDLE
	mov cout, EAX
	mov EAX, 5d
	mov EBX, zmiennaa
	mul EBX
	mov wynik, EAX

	mov EAX, 4d
	imul EAX, zmiennab

	sub EAX, zmiennac
	add wynik,EAX
	
	push wynik
	push OFFSET tekst
	push OFFSET bufor
	call wsprintfA


	push 0
	push OFFSET rout
	push rozmiar
	push  OFFSET bufor
	push cout
	call WriteConsoleA


	invoke  ExitProcess,0
main endp

END